package com.example.wiki.job;

import com.example.wiki.service.DocService;
import com.example.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DocJob {

    private static final Logger LOG = LoggerFactory.getLogger(DocJob.class);

    @Autowired
    DocService docService;

    @Autowired
    SnowFlake snowFlake;

    /**
     * 每10分钟执行一次
     */
    @Scheduled(cron = "0 0/10 * * * ? ")
    public void updateEbookForDoc(){
        // 增加日志流水号 用于查看日志 过滤日志 方便查看于维护
        MDC.put("LOG_ID", String.valueOf(snowFlake.nextId()));

        docService.updateEbookForDoc();
        LOG.info("文档定时任务:更新电子书!");
    }

}
