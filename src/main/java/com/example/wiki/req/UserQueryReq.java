package com.example.wiki.req;

import com.example.wiki.common.req.CommonReq;

public class UserQueryReq extends CommonReq {

    private  String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
