package com.example.wiki.service.impl;

import com.example.wiki.mapper.EbookSnapshotMapper;
import com.example.wiki.vo.StatisticVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClasName EbookSnapshotService
 * @Descripution TODO
 * @Author leikw
 * @Date 2021/9/29 14:56
 * @Version V1.0
 */
@Service
public class EbookSnapshotService {

    @Autowired
    EbookSnapshotMapper ebookSnapshotMapper;

    public void genSnapshot() {
        ebookSnapshotMapper.genSnapshot();
    }

    public List<StatisticVo> getStatisticVo(){
      return ebookSnapshotMapper.getStatisticVo();
    }

    public List<StatisticVo> get30Statistic() {
        return ebookSnapshotMapper.get30Statistic();
    }
}
