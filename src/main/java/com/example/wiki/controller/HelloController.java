package com.example.wiki.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${test.name}")
    private  String hname;

    @GetMapping("/hello")
    public String sayHello(){
        return "你好呀！";
    }

    @PostMapping("/hello/post")
    public String sayHellopost(String name){
        return "你好呀 "+hname+"  !";
    }


}
