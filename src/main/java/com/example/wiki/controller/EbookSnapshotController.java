package com.example.wiki.controller;

import com.example.wiki.common.vo.CommonVo;
import com.example.wiki.req.EbookQueryReq;
import com.example.wiki.req.EbookSaveReq;
import com.example.wiki.service.impl.EbookSnapshotService;
import com.example.wiki.vo.EbookVo;
import com.example.wiki.common.vo.PageVo;
import com.example.wiki.service.EbookService;
import com.example.wiki.vo.StatisticVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("ebookSnapshot")
public class EbookSnapshotController {

    @Autowired
    EbookSnapshotService ebookSnapshotService;

    @GetMapping("/get_statistic")
    public CommonVo<List<StatisticVo> > getStatistic() {
        List<StatisticVo> statisticResp = ebookSnapshotService.getStatisticVo();
        CommonVo<List<StatisticVo>> commonResp = new CommonVo<>();
        commonResp.setContent(statisticResp);
        return commonResp;
    }

    @GetMapping("/get_30statistic")
    public CommonVo<List<StatisticVo> > get30Statistic() {
        List<StatisticVo> statisticResp = ebookSnapshotService.get30Statistic();
        CommonVo<List<StatisticVo>> commonResp = new CommonVo<>();
        commonResp.setContent(statisticResp);
        return commonResp;
    }
}
