package com.example.wiki.service.impl;

import com.example.wiki.common.util.CopyUtil;
import com.example.wiki.common.vo.PageVo;
import com.example.wiki.entity.User;
import com.example.wiki.entity.UserExample;
import com.example.wiki.exeception.BusinessException;
import com.example.wiki.exeception.BusinessExceptionCode;
import com.example.wiki.mapper.UserMapper;
import com.example.wiki.req.UserLoginReq;
import com.example.wiki.req.UserQueryReq;
import com.example.wiki.req.UserResetPasswordReq;
import com.example.wiki.req.UserSaveReq;
import com.example.wiki.service.UserService;
import com.example.wiki.util.SnowFlake;
import com.example.wiki.vo.UserLoginVo;
import com.example.wiki.vo.UserVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;
import sun.rmi.runtime.Log;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    private final static Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserMapper userMapper;

    @Autowired
    SnowFlake snowFlake;//使用雪花算法生成id

    @Override
    public PageVo<UserVo> list(UserQueryReq userQueryReq) {
        //组装sql
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        if (!ObjectUtils.isEmpty(userQueryReq.getName()))
            criteria.andNameLike("%" + userQueryReq.getName() + "%");

        //分页查询
        PageHelper.startPage(userQueryReq.getPage(), userQueryReq.getSize());
        List<User> list = userMapper.selectByExample(userExample);

        //处理数据后封装
        PageInfo<User> pageInfo = new PageInfo<>(list);
        List<UserVo> userVoList = CopyUtil.copyList(list, UserVo.class);
        PageVo<UserVo> pageVo = new PageVo<UserVo>();
        pageVo.setList(userVoList);
        pageVo.setTotal(pageInfo.getTotal());
        return pageVo;
    }

    @Override
    public void save(UserSaveReq userSaveReq) {
        User user = CopyUtil.copy(userSaveReq, User.class);
        System.out.println(user);
        if (ObjectUtils.isEmpty(user.getId())) {

            User selectUser = selectByLoginName(user.getLoginName());

            if(selectUser==null){
                user.setId(snowFlake.nextId());
                user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
                userMapper.insert(user);
            }else{
                //用户名已存在
                throw new BusinessException(BusinessExceptionCode.USER_LOGIN_NAME_EXIST);
            }
        } else {
            user.setLoginName(null);
            user.setPassword(null);
            userMapper.updateByPrimaryKeySelective(user);
        }
    }

    @Override
    public void remove(Long id) {
        if (id != null) {
            userMapper.deleteByPrimaryKey(id);
        }
    }

    public User selectByLoginName(String loginName) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andLoginNameEqualTo(loginName);

        List<User> list = userMapper.selectByExample(userExample);
        if (list.size()==0) {
            return null;
        } else {
            return list.get(0);
        }
    }


    @Override
    public void resetPassword(UserResetPasswordReq userResetPasswordReq) {
        User user = CopyUtil.copy(userResetPasswordReq, User.class);
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public UserLoginVo login(UserLoginReq userLoginReq) {
        //两层加密
        userLoginReq.setPassword(DigestUtils.md5DigestAsHex(userLoginReq.getPassword().getBytes()));

        User selectByLoginName = selectByLoginName(userLoginReq.getLoginName());
        //全都抛出 LOGIN_USER_ERROR 一个异常  前台得到模糊的提示 防止黑客可以判断 用户名是否存在或者密码错误
        if(selectByLoginName==null){
            //用户名不存在
            LOG.info("用户名不存在 {}",userLoginReq.getLoginName());//后台可以看到日志记录
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }else if(! selectByLoginName.getPassword().equals(userLoginReq.getPassword())){
            //密码错误
            LOG.info("用户密码错误 {}",userLoginReq.getPassword());//后台可以看到日志记录
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }else{
            return CopyUtil.copy(selectByLoginName,UserLoginVo.class);
        }

    }
}
