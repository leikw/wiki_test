import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import {Tool} from "@/util/tool";
import store from "@/store";

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/admin/ebook',
    name: 'AdminEbook',
    component: () => import('../views/admin/AdminEbook.vue')//懒加载
    ,meta:{
      loginRequire:true
    }
  },
  {
    path: '/admin/category',
    name: 'AdminCategory',
    component: () => import('../views/admin/AdminCategory.vue')//懒加载
    ,meta:{
      loginRequire:true
    }
  },
  {
    path: '/admin/doc',
    name: 'AdminDoc',
    component: () => import('../views/admin/AdminDoc.vue')//懒加载
    ,meta:{
      loginRequire:true
    }
  },
  {
    path: '/doc',
    name: 'Doc',
    component: () => import('../views/Doc.vue')//懒加载
  },
  {
    path: '/admin/user',
    name: 'AdminUser',
    component: () => import('../views/admin/AdminUser.vue')//懒加载
    ,meta:{
      loginRequire:true
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})


// 路由登录拦截
router.beforeEach((to, from, next) => {
  // 要不要对meta.loginRequire属性做监控拦截
  if (to.matched.some(function (item) {
    console.log(item, "是否需要登录校验：", item.meta.loginRequire);
    return item.meta.loginRequire
  })) {
    const loginUser = store.state.user;
    if (Tool.isEmpty(loginUser)) {
      console.log("用户未登录！");
      next('/');
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router
