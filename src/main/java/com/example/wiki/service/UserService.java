package com.example.wiki.service;

import com.example.wiki.common.vo.PageVo;
import com.example.wiki.req.UserLoginReq;
import com.example.wiki.req.UserQueryReq;
import com.example.wiki.req.UserResetPasswordReq;
import com.example.wiki.req.UserSaveReq;
import com.example.wiki.vo.UserLoginVo;
import com.example.wiki.vo.UserVo;

public interface UserService {

    public PageVo<UserVo> list(UserQueryReq userQueryReq);

   public  void save(UserSaveReq userSaveReq);

    public void remove(Long id);

    public  void resetPassword(UserResetPasswordReq userResetPasswordReq);

    UserLoginVo login(UserLoginReq userLoginReq);
}
