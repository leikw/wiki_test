package com.example.wiki.req;

import com.example.wiki.common.req.CommonReq;

public class EbookQueryReq extends CommonReq {

    private  String name;

    private Long category2Id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Long getCategory2Id() {
        return category2Id;
    }

    public void setCategory2Id(Long category2Id) {
        this.category2Id = category2Id;
    }
}
