package com.example.wiki.controller;

import com.example.wiki.common.vo.CommonVo;
import com.example.wiki.common.vo.PageVo;
import com.example.wiki.req.CategoryQueryReq;
import com.example.wiki.req.CategorySaveReq;
import com.example.wiki.service.CategoryService;
import com.example.wiki.vo.CategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;


    @GetMapping("list")
    public CommonVo<PageVo<CategoryVo>> getList(@Valid CategoryQueryReq categoryQueryReq){//@Valid 检验
        PageVo<CategoryVo> pageVo = categoryService.list(categoryQueryReq);
        CommonVo<PageVo<CategoryVo>> commonVo = new CommonVo<>();
        commonVo.setContent(pageVo);
        return commonVo;
    }

    @GetMapping("all")
    public CommonVo<List<CategoryVo>> getAll(){//@Valid 检验
        List<CategoryVo> categoryVoList = categoryService.all();
        CommonVo<List<CategoryVo>> commonVo = new CommonVo<>();
        commonVo.setContent(categoryVoList);
        return commonVo;
    }

    @PostMapping("save")
    public CommonVo save( @Valid @RequestBody  CategorySaveReq categorySaveReq){

        CommonVo commonVo = new CommonVo<>();

         categoryService.save(categorySaveReq);

        return commonVo;
    }

    @DeleteMapping("delete/{id}")
    public CommonVo delete(@PathVariable Long id){

        CommonVo commonVo = new CommonVo<>();

        categoryService.remove(id);

        return commonVo;
    }


}
