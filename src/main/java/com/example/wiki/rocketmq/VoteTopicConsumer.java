//package com.example.wiki.rocketmq;
//
//import com.example.wiki.service.impl.WebSocketService;
//import com.example.wiki.websocket.WebSocketServer;
//import org.apache.rocketmq.common.message.MessageExt;
//import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
//import org.apache.rocketmq.spring.core.RocketMQListener;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
///**
// * @author leikw
// * @version 1.0
// * @description VoteTopicConsumer
// * @date 19:43
// * 消息端监听消息
// */
//
//
//@Service
//@RocketMQMessageListener(consumerGroup = "default" , topic = "Vote_Topic")
//public class VoteTopicConsumer implements RocketMQListener<MessageExt> {
//
//
//    @Autowired
//    WebSocketServer webSocketServer;
//
//    @Override
//    public void onMessage(MessageExt messageExt) {
//        byte[] body = messageExt.getBody();
//        System.out.println("rocketMq消息："+new String(body));
//        webSocketServer.sendInfo(new String(body));
//    }
//
//}
