package com.example.wiki.job;

import com.example.wiki.service.DocService;
import com.example.wiki.service.impl.EbookSnapshotService;
import com.example.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EbookSnapshotJob {

    private static final Logger LOG = LoggerFactory.getLogger(EbookSnapshotJob.class);

    @Autowired
    EbookSnapshotService ebookSnapshotService;

    @Autowired
    SnowFlake snowFlake;

    /**
     * 每10分钟执行一次
     */
    @Scheduled(cron = "0 0/10 * * * ? ")
    public void doGenSnapshot(){
        // 增加日志流水号 用于查看日志 过滤日志 方便查看于维护
        MDC.put("LOG_ID", String.valueOf(snowFlake.nextId()));

        ebookSnapshotService.genSnapshot();
        LOG.info("文档定时任务:更新统计数据快照!");
    }

}
