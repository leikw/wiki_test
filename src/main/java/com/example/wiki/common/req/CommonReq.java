package com.example.wiki.common.req;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

public class CommonReq  {

    @NotNull(message = "当前页不可为空")
    private int page;

    @NotNull(message = "每页条数不可为空")
    @Max(value = 1000,message = "每页条数不可超过1000")
    private int size;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
