package com.example.wiki.controller;

import com.example.wiki.common.vo.CommonVo;
import com.example.wiki.req.EbookQueryReq;
import com.example.wiki.req.EbookSaveReq;
import com.example.wiki.vo.EbookVo;
import com.example.wiki.common.vo.PageVo;
import com.example.wiki.service.EbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("ebook")
public class EbookController {

    @Autowired
    EbookService ebookService;


    @GetMapping("list")
    public CommonVo<PageVo<EbookVo>> getList(@Valid EbookQueryReq ebookQueryReq){//@Valid 检验
        PageVo<EbookVo> pageVo = ebookService.list(ebookQueryReq);
        CommonVo<PageVo<EbookVo>> commonVo = new CommonVo<>();
        commonVo.setContent(pageVo);
        return commonVo;
    }

    @PostMapping("save")
    public CommonVo save( @Valid @RequestBody  EbookSaveReq ebookSaveReq){

        CommonVo commonVo = new CommonVo<>();

         ebookService.save(ebookSaveReq);

        return commonVo;
    }

    @DeleteMapping("delete/{id}")
    public CommonVo delete(@PathVariable Long id){

        CommonVo commonVo = new CommonVo<>();

        ebookService.remove(id);

        return commonVo;
    }


}
