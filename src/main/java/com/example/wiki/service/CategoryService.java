package com.example.wiki.service;

import com.example.wiki.common.vo.PageVo;
import com.example.wiki.req.CategoryQueryReq;
import com.example.wiki.req.CategorySaveReq;
import com.example.wiki.vo.CategoryVo;

import java.util.List;

public interface CategoryService {

    public PageVo<CategoryVo> list(CategoryQueryReq categoryQueryReq);

    public List<CategoryVo> all();

    public  void save(CategorySaveReq categorySaveReq);

    public void remove(Long id);
}
