package com.example.wiki.service;

import com.example.wiki.common.vo.PageVo;
import com.example.wiki.entity.Content;
import com.example.wiki.req.DocQueryReq;
import com.example.wiki.req.DocSaveReq;
import com.example.wiki.vo.DocVo;

import java.util.List;

public interface DocService {

    public PageVo<DocVo> list(DocQueryReq docQueryReq);

    public List<DocVo> all(Long ebookId);

    public  void save(DocSaveReq docSaveReq);

    public void remove(Long id);

    void remove(List<String> asList);

    Content findContent(Long id);

    void vote(Long id);

    void updateEbookForDoc();
}
