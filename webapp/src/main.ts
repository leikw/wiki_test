import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import * as Icons from '@ant-design/icons-vue';
import axios from 'axios';//引入axios
import store from "@/store";
import {Tool} from "@/util/tool";


const app =createApp(App);

app.use(router).use(Antd).mount('#app')

/**
 *  app全局引入图标
 */
const icon : any  =Icons;
for (const  i in icon){  //for ... 遍历对象属性
 app.component(i,icon[i]);
}

/**
 * 读取环境参数
 */
console.log("VUE_APP_SERVER："+process.env.VUE_APP_SERVER)
console.log("NODE_ENV："+process.env.NODE_ENV )


/**
 * 设置axios baseURL
 */
axios.defaults.baseURL=process.env.VUE_APP_SERVER;

/**
 * axios拦截器
 */
axios.interceptors.request.use(function (config) {
   const token =store.state.user.token;
   if(Tool.isNotEmpty(token)){
      config.headers.token = token;
      console.log('请求参数头增加token：', token);
   }
   console.log('请求参数：', config);
   return config;
}, error => {
   console.log('返回错误：', error);
   return Promise.reject(error);
});
axios.interceptors.response.use(function (response) {
   console.log('返回结果：', response);
   return response;
}, error => {
   console.log('返回错误：', error);
   return Promise.reject(error);
});
