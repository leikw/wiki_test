package com.example.wiki.service.impl;

import com.example.wiki.websocket.WebSocketServer;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class WebSocketService {

    @Autowired
    WebSocketServer webSocketServer;

    @Async   //执行后查看日志 发现异步化成功         // Async-3
    public void  sendInfo(String msg,String log_id){
        MDC.put("LOG_ID",log_id);
        //点赞后推送消息
        webSocketServer.sendInfo(msg);
    }
}
