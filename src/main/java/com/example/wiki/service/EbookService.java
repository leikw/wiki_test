package com.example.wiki.service;

import com.example.wiki.common.vo.PageVo;
import com.example.wiki.req.EbookQueryReq;
import com.example.wiki.req.EbookSaveReq;
import com.example.wiki.vo.EbookVo;

public interface EbookService {

    public PageVo<EbookVo> list(EbookQueryReq ebookQueryReq);

   public  void save(EbookSaveReq ebookSaveReq);

    public void remove(Long id);
}
