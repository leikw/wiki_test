package com.example.wiki.req;

import com.example.wiki.common.req.CommonReq;

public class DocQueryReq extends CommonReq {

    @Override
    public String toString() {
        return "DocQueryReq{} " + super.toString();
    }
}
