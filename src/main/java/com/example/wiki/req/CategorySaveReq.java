package com.example.wiki.req;

import javax.validation.constraints.NotNull;

public class CategorySaveReq {
    private Long id;

    private Long parenet;

    @NotNull(message = "分类名不可为空")
    private String name;

    @NotNull(message = "排序值不可为空")
    private Integer sort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParenet() {
        return parenet;
    }

    public void setParenet(Long parenet) {
        this.parenet = parenet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parenet=").append(parenet);
        sb.append(", name=").append(name);
        sb.append(", sort=").append(sort);
        sb.append("]");
        return sb.toString();
    }
}
