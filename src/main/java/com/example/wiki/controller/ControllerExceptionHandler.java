package com.example.wiki.controller;

import com.example.wiki.common.vo.CommonVo;
import com.example.wiki.exeception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理、数据预处理等
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    /**
     * 校验异常统一处理
     * @param e
     * @return
     */
    @ExceptionHandler(value = BindException.class)
    @ResponseBody
    public CommonVo validExceptionHandler(BindException e) {
        CommonVo commonResp = new CommonVo();
        LOG.warn("参数校验失败：{}", e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        commonResp.setSuccess(false);
        commonResp.setMessage(e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        return commonResp;
    }


    /**
     * 校验异常统一处理
     * @param e
     * @return
     */
    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public CommonVo validExceptionHandler(BusinessException e) {
        CommonVo commonVo = new CommonVo();
        LOG.warn("业务异常：{}", e.getCode().getDesc());
        commonVo.setSuccess(false);
        commonVo.setMessage(e.getCode().getDesc());
        return commonVo;
    }


    /**
     * 校验异常统一处理
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public CommonVo validExceptionHandler(Exception e) {
        CommonVo commonVo = new CommonVo();
        LOG.error("业务异常：", e);
        commonVo.setSuccess(false);
        commonVo.setMessage("系统出现异常，请联系管理员！");
        return commonVo;
    }


}
