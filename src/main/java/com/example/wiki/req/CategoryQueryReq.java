package com.example.wiki.req;

import com.example.wiki.common.req.CommonReq;

public class CategoryQueryReq extends CommonReq {

    @Override
    public String toString() {
        return "CategoryQueryReq{} " + super.toString();
    }
}
