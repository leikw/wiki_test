package com.example.wiki.controller;

import com.example.wiki.common.vo.CommonVo;
import com.example.wiki.common.vo.PageVo;
import com.example.wiki.entity.Content;
import com.example.wiki.req.DocQueryReq;
import com.example.wiki.req.DocSaveReq;
import com.example.wiki.service.DocService;
import com.example.wiki.vo.DocVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("doc")
public class DocController {

    @Autowired
    DocService docService;


    @GetMapping("list")
    public CommonVo<PageVo<DocVo>> getList(@Valid DocQueryReq docQueryReq){//@Valid 检验
        PageVo<DocVo> pageVo = docService.list(docQueryReq);
        CommonVo<PageVo<DocVo>> commonVo = new CommonVo<>();
        commonVo.setContent(pageVo);
        return commonVo;
    }

    @GetMapping("all/{ebookId}")
    public CommonVo<List<DocVo>> getAll(@PathVariable Long ebookId){//@Valid 检验
        List<DocVo> docVoList = docService.all(ebookId);
        CommonVo<List<DocVo>> commonVo = new CommonVo<>();
        commonVo.setContent(docVoList);
        return commonVo;
    }

    @PostMapping("save")
    public CommonVo save( @Valid @RequestBody  DocSaveReq docSaveReq){

        CommonVo commonVo = new CommonVo<>();

         docService.save(docSaveReq);

        return commonVo;
    }

    @DeleteMapping("delete/{idsStr}")
    public CommonVo delete(@PathVariable String idsStr){

        CommonVo commonVo = new CommonVo<>();

        List<String> idList = Arrays.asList(idsStr.split(","));
        docService.remove(idList);

        return commonVo;
    }

    @GetMapping("find_content/{id}")
    public CommonVo<Content> findContent(@PathVariable Long id){

        CommonVo commonVo = new CommonVo<>();

        Content content = docService.findContent(id);

        commonVo.setContent(content);
        return commonVo;
    }

    @GetMapping("vote/{id}")
    public CommonVo vote(@PathVariable Long id){

        CommonVo commonVo = new CommonVo<>();

        docService.vote(id);

        return commonVo;
    }

}
