
//Vuex 是一个专为 Vue.js 应用程序开发的状态管理模式。
// 它采用集中式存储管理应用的所有组件的状态
// ，并以相应的规则保证状态以一种可预测的方式发生变化。
// Vuex 也集成到 Vue 的官方调试工具 devtools extension (opens new window)，提供了诸如
// 零配置的 time-travel 调试、状态快照导入导出等高级调试功能
import { createStore } from 'vuex';

//声明 SessionStorage 已存在
declare let SessionStorage :any;
const USER = "user"

const store = createStore({
  state: {
      user: SessionStorage.get(USER) || {}
  },
  mutations: {
      //state 自带参数  user 传入参数
      setUser ( state : any , user : any){
        state.user =user;
          SessionStorage.set(USER,user);
      }
  },
  actions: {
  },
  modules: {

  }
});

export default store;
